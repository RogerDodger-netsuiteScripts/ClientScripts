/**
 * Description: Sends email of solution to original sender once case is closed and saved.
 @NApiVersion 2.0
 @NScriptType ClientScript
 @NModuleScope Public
 */
define(['N/email', 'N/runtime','N/currentRecord'], function (email, runtime, cr) {

    function emailSolution(context) {

        var currentRecord = cr.get();

        // Map each Record Type (condition) to the appropriate function (handler)
        var eventRouter = {
            customrecord_netsuite_tickets: nsTicketHandler,
            customrecord_it_troubletickets: itTicketHandler,
            customrecord_website_tickets: siteTicketHandler
        };

        // Retrieve the appropriate function based on record type
        var fn = eventRouter[currentRecord.type];

        // Nothing to do if we don't have a route for the current record type
        if (typeof fn !== "function") {
            // Alternatively, this could perform some kind of default action/behavior
            return true;
        }

        // Invoke the retrieved function
        var data = fn(currentRecord);

        // Do nothing for closed tickets
        if (data.status !== "3") {
            return true;
        }

        var bodyText = '<div style="font-weight: bold;">Thank you for using the ticket system. Below is the solution for your reference:<br><br></div>' + data.solution;

        //Internal ID of admin from Script Parameter
        var senderId = runtime.getCurrentScript().getParameter({name: 'custscript_netsuiteadmin'});

        //Send email
        email.send({
            author: senderId,
            recipients: data.recipient,
            subject: '[Solution] ' + data.subject,
            body: bodyText,
            notifySenderOnBounce: true
        });

        //Must return true which equates to a valid record otherwise false suppresses form submission
        return true;
    }

    function nsTicketHandler(record) {
        return {
            recipient:  record.getValue({fieldId:"custrecord_nsticket_employee"}),
            solution:   record.getValue({fieldId:"custrecord_nsticket_solution"}),
            status:     record.getValue({fieldId:"custrecord_nsticket_status"}),
            subject:    record.getValue({fieldId:"custrecord_nsticket_subject"})
        };
    }

    function itTicketHandler(record) {
        return {
            recipient:  record.getValue({fieldId:"custrecord_it_employee"}),
            solution:   record.getValue({fieldId:"custrecord_it_solution"}),
            status:     record.getValue({fieldId:"custrecord_it_status"}),
            subject:    record.getValue({fieldId:"custrecord_it_subject"})
        };
    }

    function siteTicketHandler(record) {
        return {
            recipient:  record.getValue({fieldId:"custrecord_websiteticket_employee"}),
            solution:   record.getValue({fieldId:"custrecord_websiteticket_solution"}),
            status:     record.getValue({fieldId:"custrecord_websiteticket_status"}),
            subject:    record.getValue({fieldId:"custrecord_websiteticket_subject"})
        };
    }

    return {
        saveRecord: emailSolution
    };
});
